<?php

class weatherForecastModel
{

    public function __construct()
    {

    }

    public function getMainData($add_details = null) {
        $dbh = Base::getDbInstance();

        try
        {
            $stmt = $dbh->prepare("SELECT * FROM spots");
            $stmt->execute();
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        catch (Exception $e)
        {
            return array('OK' => 0, 'Error' => 'Something went wrong with the update');
        }

        return $results;
    }

    public function getWeatherDetails($spot = null, $add_data = null) {

        $additional_statements = null;

        if($add_data && $add_data == 'next_hours') {
            if($spot)
                $additional_statements = " AND ";
            else
                $additional_statements = " WHERE ";

            $additional_statements .= "
                (
                    (date = curdate() AND time > if (hour(NOW()) = 0, hour(NOW()), CONCAT(hour(NOW()), '00')))
                    OR
                    (date <= DATE_ADD(curdate(), INTERVAL 1 DAY) AND date > curdate())
                ) ORDER BY spot_id ASC, date, time ASC";
        }

        $dbh = Base::getDbInstance();
        try
        {
            if($spot) {
                $stmt = $dbh->prepare("SELECT f.* FROM forecast AS f WHERE spot_id = :spot_id".$additional_statements);
                $stmt->bindParam(':spot_id', $spot, PDO::PARAM_INT);
            } else {
                $stmt = $dbh->prepare("SELECT f.* FROM forecast AS f".$additional_statements);
            }

            $stmt->execute();
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        catch (Exception $e)
        {
            return array('OK' => 0, 'Error' => 'Something went wrong with the update');
        }

        return $results;
    }

    public function updateData() {

        $spotsData = $this->getMainData();
        $base_url = 'http://api.worldweatheronline.com/premium/v1/marine.ashx';
        $weather_api_params = array(
            'q' => '0.000,0.000',
            'format' => 'json',
            'key' => 'e6ff2f299379a12da1dc4cedfda84fbc8116c8ef'
        );
        $dbh = Base::getDbInstance();

        foreach($spotsData AS $spotData) {
            $weather_api_params['q'] = $spotData['latitude'] .','.$spotData['longitude'];

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $base_url .'?'. http_build_query($weather_api_params)
            ));

            $resp = curl_exec($curl);

            curl_close($curl);

            foreach(json_decode($resp) AS $res) {
                foreach($res as $mainK => $mainValue) {
                    if($mainK == "weather") {
                        foreach($mainValue as $weatherData) {
                            foreach($weatherData as $weatherKey => $weatherValue) {
                                if($weatherKey == "hourly") {
                                    foreach($weatherValue AS $hourlyData) {
                                        //Normalization
                                        $hourlyData->weatherDesc = $hourlyData->weatherDesc[0]->value;
                                        $hourlyData->weatherIconUrl = $hourlyData->weatherIconUrl[0]->value;

                                        //Prepare for the db INSERT
                                        $insertData = get_object_vars($hourlyData);

                                        $fields=array_keys($insertData);
                                        $values=array_values($insertData);

                                        $fields_list=implode(',',$fields);
                                        $query_strings=str_repeat("?,",count($fields));

                                        $sql="REPLACE INTO forecast($fields_list,last_update,spot_id, date) values(${query_strings} NOW(), ". $spotData['spot_id'] .",'". $weatherData->date ."')";
                                        $query=$dbh->prepare($sql);
                                        $query->execute($values);

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}