There are 23 milion surfers worldwide.
Annual Surfing industry revenue is $7.29 billion!
There are websites providing weather information for surfers.
But, no really great apps, which are nice looking, crossplatform and have info on all surf spots worldwide.

Our goal for this hackathon was to create MVP: App that will add value to Bulgarian surfers by providing info for surf spots on the Bulgarian coast.

Target is relativelly small, but value for them would be great.
We will listen and iterate.
Next steps would be to cover whole Black Sea and other regions which are not found on other apps.
Iterate.
World coverage.

Starting from final brainstorming to the DEMO build, all was done during the hackathon.