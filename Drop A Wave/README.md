Setup project:

Project uses GUARD - https://github.com/guard/guard
To install and run ( use sudo for mac users ) - gem install guard guard-sass guard-jammit

Docs for guard - https://github.com/guard/guard
Docs for guard-sass - https://github.com/hawx/guard-sass
Docs for guard-jammit - http://documentcloud.github.io/jammit/

To run guard just type - guard
To exit guard just type - exit

CSS:
Display
Positioning
Transforms
Width and height
Margins and paddings
Borders (and others, affecting element's total dimensions)
Backgrounds, aligning, transitions (and others, which don't affect whole page's layout)
Typography