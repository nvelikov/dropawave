(function($) {
    dwApp = {
        initialize: function() {
            this.cacheTemplates();
            this.bindEvents();
        },
        bindEvents: function() {
            this.receivedEvent('deviceready');
            //document.addEventListener('deviceready', this.onDeviceReady, false);
        },
        cacheTemplates: function() {
            $.tpl = {};
        
            $('script.template').each(function(index) {
                var $this = $(this);
                $.tpl[$this.attr('id')] = _.template($this.html());
                $this.remove();
            });
        },
        // deviceready Event Handler
        //
        // The scope of 'this' is the event. In order to call the 'receivedEvent'
        // function, we must explicity call 'app.receivedEvent(...);'
        onDeviceReady: function() {
            dwApp.receivedEvent('deviceready');
            //navigator.splashscreen.hide();
        },
        // Update DOM on a Received Event
        receivedEvent: function(id) {
            dwApp.localStorage = new LocalStorageHelper();
            dwApp.rest = new RestHelper();
            if(!window.device || !window.device.uuid) {
                window.device = {
                    uuid: 1
                };
            }
            if(id == "deviceready") {
                dwApp.user = new UserModel({'uuid':window.device.uuid});
            }

            
            dwApp.router = new Router({
                '': function() {
                },
                'home\/$': function() {
                    console.log('ROUTER: home');
                },
                'spots\/$': function() {
                    console.log('ROUTER: spots');
                },
                'spots\/([0-9]*)\/$': function() {
                    console.log('ROUTER: spots/{id}');
                },
                'favourites\/$': function() {
                    console.log('ROUTER: favourites');
                }
            });
            
            NavigationView.initialize();
            NavigationView.animationState().one();
            
            var promiseLoadSpots = new $.Deferred();
            var promiseLoadForecast = new $.Deferred();
            
            dwApp.rest.request('', function(data) {
                if (data.success !== true) {
                    promiseLoadSpots.reject();
                }
                
                dwApp.localStorage.setItem('spots', data.result);
                promiseLoadSpots.resolve();
            }, {data: {action: 'getMainData', id: ''}});
            
            dwApp.rest.request('', function(data) {
                if (data.success !== true) {
                    promiseLoadForecast.reject();
                }
                
                dwApp.localStorage.setItem('forecast', data.result);
                promiseLoadForecast.resolve();
            }, {data: {action: 'getWeather'}});
            
            $.when(promiseLoadSpots, promiseLoadForecast)
            .then(function() {
                SpotsModel.setForecast();
                // delete forecast data after sorted and appended to the spots data
                dwApp.localStorage.setItem('forecast', null);
                
                setTimeout(function() {
                    // load first navigation animation
                    NavigationView.animationState().two();
                    
                    // Start loading pages
                    dwApp.router.initialize();
                }, 3000);
            })
            .fail(function() {
                alert('Please check your internet connection!');
            });
        },
        
        loadPage: function() {
            alert('loaded');
        },
        
        config : {
            appName: 'Drop A Wave',
            appVersion: 0.1,
            language: 'en',
            lsPrefix: 'dw',
            resources: {
                i18n: "resources/i18n/",
                baseUrl: "http://www.dropawave.com/API/index.php"
            },
            request: {
                timeout: 10000,
                defaultMethod: "GET",
                dataType: "json"
            }
        }
    };
    
    $(document).ready(function() {
        dwApp.initialize();
    });
    
    
    
    
    // Local Storage Helper
    var LocalStorageHelper = function(options) {
        var settings = $.extend({
            preffix: dwApp.config.lsPrefix
        }, options);
    
        this.getItem = function(key) {
            return JSON.parse(localStorage.getItem(settings.preffix + '_' + key));
        },
        
        this.setItem = function(key, value) {
            try { 
                localStorage.setItem(settings.preffix + '_' + key, JSON.stringify(value));
            } catch(e) {
                this.catchLocalStorageError(e);
            }
            
        },
        
        this.hasItem = function(key) {
            return this.getItem(key) !== null;
        },
    
        this.clear = function() {
            localStorage.clear();
        },
        
        this.catchStorageError = function(e) {
            if (e.code === DOMException.QUOTA_EXCEEDED_ERR && localStorage.length === 0) {
                return false;
            }
            else {
                throw e;
            }
        }
    };
    
    var RestHelper = function(options) {
        var settings = {
            url: dwApp.config.resources.baseUrl,
            type: dwApp.config.request.defaultMethod,
            dataType: dwApp.config.request.dataType,
            timeout: dwApp.config.request.timeout
        };
        
        $.extend(settings, options);
        
        this.request = function(url, callback, options) {
            var ajaxOptions = settings;
            
            $.extend(ajaxOptions, options);
            ajaxOptions.success = callback;
            
            return $.ajax(ajaxOptions);
        }
    }
    
    var Router = function(map) {
        var history = [],
            pageMap = map;
        
        this.initialize = function() {
            this.addHistoryItem(window.location.hash);
            this.onHashChange();
            $(window).on('hashchange', this.onHashChange);
        },
        
        this.onHashChange = function() {
            var hash = window.location.hash;
            hash = hash.replace('#', '');
            history.push(hash);
            
            $.each(pageMap, function(key, urlCallback) {
                var regEx = new RegExp(key);
                
                if (hash.match(key)) {
                    urlCallback();
                }
            });
        },
        
        this.addHistoryItem = function(hash) {
            history.push(hash);
        },
        
        this.getHistory = function() {
            return history;
        },
        
        this.getLastHistory = function() {
            return history.pop();
        }
    }
    
    /**
     * Views
     *
     *
     */
    var NavigationView = {
        elm: '#navigation',
        splashElm: '#splash',
        menuButton: '#fake_button',
        mainMenu: '.main-nav',
        initialize: function() {
            this.bindEvents();
        },
        bindEvents: function() {
            var _this = this;
            $(this.elm).on('click', '.hexagon', function(e) {
                e.preventDefault();
                
                var choiceIndex = $(this).data('category');
                
                dwApp.user.set('category_choice', choiceIndex);
                
                //_this.animationState();
                //$(_this.elm).fadeOut();
                $(_this.splashElm).fadeOut();
                _this.animationState().three();
                HomePageView.render();
            });
            
            $(this.elm).on('click', this.menuButton, function(e) {
                e.preventDefault();
                
                if ($(this).hasClass('opened')) {
                    _this.animationState().close();
                }
                else {
                    _this.animationState().open();
                }
                
                $(this).toggleClass('opened');
            });
            
            $(this.mainMenu).on('click', 'a', function(e) {
                e.preventDefault();
                e.stopPropagation();
                var menuChoice = $(this).data('title');
                
                if (menuChoice === 'home') {
                    HomePageView.render();
                }
                else if (menuChoice === 'spots') {
                    SpotsPageListView.render();
                }
                else if (menuChoice === 'events') {
                    // Load events page. Due to short schedule there was no time to develop this
                }
                
                $(_this.menuButton).trigger('click');
            });
        },
        render: function() {
        },
        animationState: function() {
            var nav = $('#navigation'),
                ocean = nav.find('> ul.ocean'),
                waves = ocean.find('> li'),
                copy = nav.find('.copy'),
                hexas = ocean.find('.hexagon')
                circles = nav.find('.circles'),
                circlesElms = circles.find('span'),
                notFakeB = nav.find('.nav-toggle'),
                fakeB = $('#fake_button'),
                menuButtons = nav.find('.main-nav a');

            this.one = function() {
                var first = true;

                copy.add(hexas).add(fakeB).hide();

                for (var i = waves.length - 1; i >= 1; i--) {

                    var _this = $(waves[i]),
                        delay = 0.2 * i,
                        pos =  _this.data('step-one');

                    if (typeof pos === 'undefined') {
                        continue;
                    }

                    if(first) {

                        TweenLite.to(_this, 0.3, {y: -pos, delay: delay, onComplete: function() {
                            nav.find('.copy').fadeIn();
                        }});

                        first = false;

                    } else {
                        TweenLite.to(_this, 0.3, {y: -pos, delay: delay});
                    }
                };

                return true;
            }

            this.two = function() {
                
                if (copy.is(':visible')) {
                    copy.fadeOut();
                }

                if (!hexas.is(':visible')) {
                    hexas.show();
                }

                $('#appLogo').fadeOut();

                TweenLite.to($('.choise'), 0.3, {y: -480, delay: 1});


                animateWaves();
                animateCirles();
                
                function animateWaves() {

                    for (var i = waves.length - 1; i >= 0; i--) {
                        var _this = $(waves[i]),
                            delay = 0.2 / (i + 1),
                            pos = _this.data('step-two'),
                            thisHexa = _this.find('.hexagon');

                        if (typeof pos === 'undefined') {
                            continue;
                        }

                        TweenLite.to(_this, 0.3, {y: -pos, delay: delay, onComplete: function(el, d) {
                            if ($(el).length) {
                                TweenLite.to(el, 0.5, {y: '-225', delay: d});
                            }
                        }, onCompleteParams: [thisHexa, delay]});
                    }
                }


                function animateCirles() {
                    var circlesPos = circles.data('step-two'),
                        circlesD = 0.5;

                    TweenLite.to(circles, 0.3, {y: -circlesPos, delay: circlesD});

                    for (var i = circlesElms.length - 1; i >= 0; i--) {
                        var el = $(circlesElms[i]),
                            circle_size = el.data('settings');

                        TweenLite.to(el, 0.3, {width: circle_size, height: circle_size, opacity: 1, delay: Math.random()});
                    };
                }
            }

            this.three = function() {

                rearangeElements();
                hideCircles();
                
                TweenLite.to($('.choise'), 0.2, {y: 0});
                LoaderView.hide();
                
                function rearangeElements() {
                    for (var i = waves.length - 1; i >= 0; i--) {
                        var _this = $(waves[i]),
                            pos = _this.data('step-three'),
                            thisHexa = _this.find('.hexagon'),
                            navButton = _this.find('.nav-toggle');

                        if (thisHexa.length) {
                            TweenLite.to(thisHexa, 0.3, {y: 0, onComplete: function(el, p) {
                                TweenLite.to(el, 0.3, {y: -p});
                            }, onCompleteParams: [_this, pos]});
                        } else {

                            if (navButton.length) {
                                TweenLite.to(_this, 0.3, {y: -pos, onComplete: function(){
                                    fakeB.show();
                                }});
                            } else {
                                TweenLite.to(_this, 0.3, {y: -pos});
                            }

                        }
                    };
                }

                function hideCircles() {
                    var circlesPos = circles.data('step-three');

                    TweenLite.to(circles, 0.3, {y: -circlesPos});

                    for (var i = circlesElms.length - 1; i >= 0; i--) {
                        var el = $(circlesElms[i]),
                            circle_size = el.data('close'),
                            delay = Math.random()/2;

                        TweenLite.to(el, 0.2, {width: circle_size, height: circle_size, opacity: 0, delay: delay});
                    };
                }
            }

            this.open = function() {
                animateCirles();
                arangeButtons();

                function animateCirles() {
                    var circlesPos = circles.data('step-open'),
                        circlesD = 0.5;

                    TweenLite.to(circles, 0.3, {y: -circlesPos, delay: circlesD});

                    for (var i = circlesElms.length - 1; i >= 0; i--) {
                        var el = $(circlesElms[i]),
                            circle_size = el.data('open');

                        TweenLite.to(el, 0.3, {width: circle_size, height: circle_size, opacity: 1, delay: Math.random()});
                    };
                }

                function arangeButtons() {
                    menuButtons.each(function() {
                        var _this = $(this),
                            _thisX = _this.data('x'),
                            _thisY = _this.data('y'),
                            _thisXOne = _this.data('x-one'),
                            _thisYOne = _this.data('y-one'),
                            _thisXTwo = _this.data('x-two'),
                            _thisYTwo = _this.data('y-two');


                        TweenMax.to(_this, 0.4, {bezier:[{x:0, y:0}, {x:_thisXOne, y:_thisYOne}, {x:_thisXTwo, y:_thisYTwo}, {x:_thisX, y:_thisY}], ease:Linear.easeNone, delay: 1});
                    });



                    TweenLite.to(notFakeB, 0.3, {y: -35, delay: Math.random()});
                }
            }

            this.close = function() {
                hideCircles();
                arangeButtons();

                function hideCircles() {
                    var circlesPos = circles.data('step-close');

                    TweenLite.to(circles, 0.3, {y: -circlesPos});

                    for (var i = circlesElms.length - 1; i >= 0; i--) {
                        var el = $(circlesElms[i]),
                            circle_size = el.data('close'),
                            delay = Math.random()/2;

                        TweenLite.to(el, 0.2, {width: circle_size, height: circle_size, opacity: 0, delay: delay});
                    };
                }

                function arangeButtons() {
                    menuButtons.each(function() {
                        var _this = $(this),
                            _thisX = _this.data('x'),
                            _thisY = _this.data('y'),
                            _thisXOne = _this.data('x-one'),
                            _thisYOne = _this.data('y-one'),
                            _thisXTwo = _this.data('x-two'),
                            _thisYTwo = _this.data('y-two');


                        TweenMax.to(_this, 0.3, {bezier:[{x:_thisX, y:_thisY}, {x:_thisXTwo, y:_thisYTwo}, {x:_thisXOne, y:_thisYOne}, {x:0, y:0}], ease:Linear.easeNone});
                    });

                    TweenLite.to(notFakeB, 0.3, {y: 0, delay: Math.random()});
                }
            }
            
            return {
                one: this.one,
                two: this.two,
                three: this.three,
                open: this.open,
                close: this.close
            };
        }
    }
    
    var HomePageView = {
        elm: '#hpage',
        selectors: {
            item: '.spot-list-item'
        },
        isRendered: false,
        initialize: function() {
            
        },
        bindEvents: function() {
            var _this = this;
            $(this.elm).on('click', this.selectors.item, function(e) {
                e.preventDefault();
                //SpotsPageListView.render();
                var spotId = $(this).data('spot');
                
                _this.remove();
                SpotsPageView.setSpotId(spotId);
                SpotsPageView.render();
            });
        },
        fetchData: function() {
            return {
                spots: _.first(SpotsModel.getOrderedByFav(),3)
            };
        },
        render: function() {
            var $this = $(this.elm);
            $('.active-page').hide().removeClass('active-page');
            $this.addClass('active-page');
            
            if (this.isRendered) {
                $this.fadeIn();
                return;
            }
            
            var templateData = this.fetchData();
            $this.html($.tpl['home-page'](templateData)).show();
            this.bindEvents();
            this.isRendered = true;
        },
        remove: function() {
            $(this.elm).hide();
        }
    }
    
    var SpotsPageView = {
        elm: '#sp',
        isRendered: false,
        spotId: 0,
        initialize: function() {
            
        },
        setSpotId: function(spotId) {
            this.spotId = spotId;
        },
        bindEvents: function() {
            $(this.elm).on('click', 'a', function() {
                SpotsView.render();
            });
        },
        fetchData: function() {
            return SpotsModel.getById(this.spotId);
        },
        initializeChart: function() {
            var ctx = document.getElementById("chart").getContext("2d");
            window.myLine = new Chart(ctx).Line(lineChartData, {
                showScale: false,
                scaleShowLabels: true,
                responsive: true,
                showTooltips: true,

                scaleShowGridLines : false,
                scaleGridLineColor : "rgba(0,0,0,.05)",
                bezierCurve : false,
                //Boolean - Whether to show a dot for each point
                pointDot : true,

                //Number - Radius of each point dot in pixels
                pointDotRadius : 5,

                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth : 1,

                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius : 0,

                //Boolean - Whether to show a stroke for datasets
                datasetStroke : false,

                //Number - Pixel width of dataset stroke
                datasetStrokeWidth : 2,

                //Boolean - Whether to fill the dataset with a colour
                datasetFill : false,

                onAnimationComplete: function()
                {
                    console.log(myLine);
                    myLine.showTooltip(myLine.datasets[0].points, true);
                },

                tooltipFillColor: "transparent",

                tooltipTitleFontFamily: "'Tahoma', 'Geneva', sans-serif",

                tooltipYPadding: 0,

                tooltipXPadding: 10,

                tooltipFontSize: 11,

                tooltipFontColor: "#eddec3",

                tooltipTitleFontStyle: "normal",

                tooltipTemplate: "<%= value %>s",

                tooltipEvents: []
            });
        },
        render: function() {
            var $this = $(this.elm);
            $('.active-page').hide().removeClass('active-page');
            $this.addClass('active-page');
            if (this.isRendered) {
                $this.fadeIn();
                return;
            }
            
            var templateData = this.fetchData();
            
            $this.html($.tpl['spots-details-page'](templateData)).show();
            this.bindEvents();
            //this.initializeChart();
            this.isRendered = true;
        }
    }


    var SpotsPageListView = {
        elm: '#spl',
        selectors: {
            item: 'li'
        },
        isRendered: false,
        initialize: function() {
            
        },
        bindEvents: function() {
            $(this.elm).on('click', this.selectors.item, function(e) {
                e.preventDefault();
                var spotId = $(this).data('spot');
                
                SpotsPageView.setSpotId(spotId);
                SpotsPageView.render();
            });
        },
        fetchData: function() {
            return {
                spots: SpotsModel.getOrderedByFav()
            };
        },
        render: function() {
            var $this = $(this.elm);
            $('.active-page').hide().removeClass('active-page');
            $this.addClass('active-page');
            
            if (this.isRendered) {
                $this.fadeIn();
                return;
            }
            
            var templateData = this.fetchData();
            
            $this.html($.tpl['spots-page-list'](templateData)).show();
            this.bindEvents();
            this.isRendered = true;
        }
    }
    
    var LoaderView = {
        elm: '.loader',
        show: function() {
            $(this.elm).show();
        },
        hide: function() {
            $(this.elm).hide();
        }
    }
    
    /**
     * Models
     *
     *
     */
    var SpotsModel = {
        setForecast: function() {
            var spots = this.getAll();
            
            $.each(spots, function(index, item) {
                spots[index]['forecast'] = ForecastModel.sortForecastForSpot(item.spot_id);
            });
            
            dwApp.localStorage.setItem('spots', spots);
        },
        setSubscribed: function(spot) {
            var spots = this.getAll(), userSettings = dwApp.user.get('settings');

            alreadySubscribed = _.filter(userSettings.favourited, function(item) {
                return item === spot;
            });

            if ( _.isEmpty(alreadySubscribed) ) {
                userSettings.favourited.push(spot.toString());
                dwApp.user.set({'settings':userSettings});
            }
        },
        setUnsubscribed: function(spot) {
            var spots = this.getAll(), userSettings = dwApp.user.get('settings');

            alreadySubscribed = _.filter(userSettings.favourited, function(item) {
                return item == spot;
            });

            if ( !_.isEmpty(alreadySubscribed) ) {
                var keyToDel = _.indexOf(userSettings.favourited, spot.toString());
                userSettings.favourited.splice(keyToDel,1);
                dwApp.user.set({'settings':userSettings});
            }
        },
        getAll: function() {
            return dwApp.localStorage.getItem('spots');
        },
        getById: function(id) {
            var spot = {},
                spotsList = this.getAll();
                
            spot = _.filter(spotsList, function(spot) {
                return _.isArray(id) ? _.contains(id, spot.spot_id) : parseInt(spot.spot_id, 10) === id;
            });
            
            return spot;
        },
        getOrderedByFav: function() {
            var sorted = [],
                sortedKeys = [],
                spotsList = this.getAll(), 
                userSettings = dwApp.user.get('settings'),
                favorited = userSettings.favourited;

            _.each(favorited, function(val){
                var el = this.getById(val)[0];
                el.fav = 1;
                sorted.push(el);
                sortedKeys.push(val);
            }, this);

            _.each(spotsList, function(val){
                if(_.indexOf(sortedKeys, val.spot_id) < 0) {
                    sorted.push(val);
                }
            });

            return sorted;
        }
    }
    
    var ForecastModel = {
        getAll: function() {
            return dwApp.localStorage.getItem('forecast');
        },
        getByIndex: function(index) {
            var forecastList = this.getAll();
            
            if (_.isEmpty(forecastList)) {
                return {};
            }
            
            return forecastList[index];
        },
        getBySpot: function(spotId) {
            var forecast = {},
                forecastList = this.getAll();
            
            forecast = _.filter(forecastList, function(item) {
                return item.spot_id === spotId;
            });
            
            return forecast;
        },
        sortForecastForSpot: function(spotId) {
            var spotForecast = this.getBySpot(spotId);
            return _.sortBy(spotForecast, function(item) {
                return Date.parse(item.date);
            });
        }
    }
    
    var UserModel = function(recevivedData) {
        var userData = {
            uuid: 0,
            settings: {
                category_choice: 0,
                favourited: []
            }
        };
        
        this.initialize = function(data) {
            $.extend(userData, data);

            dwApp.rest.request('', function(data) {
                userData.settings = data.result.settings;
            }, {data: {action: 'userInitial', uuid: userData.uuid, settings: JSON.stringify(userData.settings)}});

        },
        
        this.set = function(key, value) {
            userData[key] = value;
            dwApp.rest.request('', function(data) {
            }, {data: {action: 'userUpdate', uuid: userData.uuid, settings: userData.settings}});
        },
        
        this.get = function(key) {
            return key ? userData[key] : userData;
        }

        this.initialize(recevivedData);
    }
})(jQuery);